package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UserHomePage {

    private WebDriver driver;
    public UserHomePage(WebDriver driver) {
        this.driver = driver;
    }
    private By clickhere = By.linkText("Click here");

    public TodoPage manageToDoApp(){
        driver.findElement(clickhere).click();
        return new TodoPage(driver);
    }

    public String verifySuccessfulLogin(){
        String verifyLogin = driver.findElement(clickhere).getText();
        return verifyLogin;
    }
}
