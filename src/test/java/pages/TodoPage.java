package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TodoPage {
    private WebDriver driver;
    public TodoPage(WebDriver driver){
        this.driver = driver;
    }
    private By addToDoElement = By.linkText("Add a Todo");


    public CreateToDOPage addToDO(){
        driver.findElement(addToDoElement).click();
        return new CreateToDOPage(driver);
    }


}
