package pages;

import org.h2.engine.User;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    private WebDriver driver;

    public LoginPage(WebDriver driver){
        this.driver = driver;
    }

    private By userNameObject = By.id("username");
    private By passwordObject = By.id("password");
    private By submitButton = By.linkText("Sign in");



    public UserHomePage validAndInvalidLogin(String username, String password){
        driver.findElement(userNameObject).sendKeys(username);
        driver.findElement(passwordObject).sendKeys(password);
        driver.findElement(submitButton).click();
        return new UserHomePage(driver);
    }
}
