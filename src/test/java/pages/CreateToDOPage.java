package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreateToDOPage {

    private WebDriver driver;
    public CreateToDOPage(WebDriver driver){
        this.driver = driver;
    }

    private By descriptionObject = By.id("desc");
    private By targetDataObject = By.id("targetDate");
    private By clickAddobject = By.xpath("//button");


    public void createToDoContent(String desc, String date){
        driver.findElement(descriptionObject).sendKeys(desc);
        driver.findElement(targetDataObject).sendKeys(date);
        driver.findElement(clickAddobject).click();


    }
}
