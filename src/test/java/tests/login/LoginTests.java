package tests.login;

import base.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.UserHomePage;

public class LoginTests extends BaseTests {

    @Test
    public void testSuccessFulLogin(){

       UserHomePage userHomePage =  loginPage.validAndInvalidLogin("in28minutes","dummy");
       String result = userHomePage.verifySuccessfulLogin();
        Assert.assertEquals(result,"Click here");

    }
}
