package solutions;

import java.util.List;
import java.util.PriorityQueue;

public class LinkedList {

    public ListNode mergeKLists(ListNode[] lists) {

        PriorityQueue<Integer> queue = new PriorityQueue<>();
        ListNode dummy = new ListNode(-1);
        ListNode output = dummy;

        for(ListNode head :lists){
            while(head != null){
                queue.add(head.val);
                head = head.next;
            }
            while(!queue.isEmpty()){
                output.next = new ListNode(queue.remove());
                output = output.next;
            }

        }
        return dummy.next;
    }
}
